module "beanstalk" {
  source = "./beanstalk"

  azs_count           = var.azs_count
  env                 = local.env
  image               = var.image
  instance_type       = var.instance_type
  module_path         = path.module
  name                = var.name
  ports               = var.ports
  private_subnets     = module.vpc.private_subnets
  public_subnets      = module.vpc.public_subnets
  region              = var.region
  use_private_subnets = var.use_private_subnets
  vpc_id              = module.vpc.id
  enable_logging      = var.enable_logging

  cname_prefix        = module.ssl.cname_prefix
}
