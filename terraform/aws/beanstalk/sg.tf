resource "aws_security_group" "instance" {
  vpc_id = var.vpc_id
  name_prefix = "${var.name}-instance-"
}
