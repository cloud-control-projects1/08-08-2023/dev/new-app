terraform {
  required_version = ">= 0.13"
}

resource "random_string" "random" {
  length  = 6
  special = false
  upper   = false
}

locals {
  prefix = random_string.random.result
  tag    = split(":", var.image)[1]
}
