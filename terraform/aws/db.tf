module "newapp_db" {
  source = "./db"

  name                                  = var.newapp_db_name
  engine                                = var.newapp_db_engine
  engine_version                        = var.newapp_db_engine_version
  instance_class                        = var.newapp_db_instance_class
  storage                               = var.newapp_db_storage
  user                                  = var.newapp_db_user
  password                              = var.newapp_db_password
  random_password                       = var.newapp_db_random_password
  vpc_id                                = module.vpc.id
  subnet_group_name                     = module.vpc.db_subnet_group_name
  multi_az                              = var.newapp_db_multi_az
  source_security_group_id              = module.beanstalk.aws_security_group.id
}
