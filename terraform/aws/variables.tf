variable "name" {
  type        = string
  default     = "newapp"
  description = "Application name"
}

variable "vpc_cidr_block" {
  type    = string
  default = "10.0.0.0/16"
}

variable "use_private_subnets" {
  type    = bool
  default = false
}

variable "azs_count" {
  type        = number
default     = 2

  description = "Number of Availability Zones to be used"
}

variable "instance_type" {
  type    = string
  default = "t2.medium"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

variable "image" {
  type    = string
}

variable "ports" {
  type    = list(number)
  default = [ 8080 ]
}

locals {
  env = [
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "NEWAPP_DB_NAME",
        value = module.newapp_db.name
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "NEWAPP_DB_HOST",
        value = module.newapp_db.host
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "NEWAPP_DB_PORT",
        value = module.newapp_db.port
      },
      {
        namespace : "aws:elasticbeanstalk:application:environment",
        name : "NEWAPP_DB_USER",
        value : module.newapp_db.username
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "NEWAPP_DB_PASSWORD",
        value = module.newapp_db.password
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "ListenerEnabled",
        value = "true"
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "Protocol",
        value = "HTTPS"
      },
      {
        namespace = "aws:elbv2:listener:443",
        name = "SSLCertificateArns",
        value = module.ssl.certificate_arn
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "StreamLogs"
        value = var.enable_logging
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "DeleteOnTerminate"
        value = var.delete_logs_on_terminate
      },
      {
        namespace = "aws:elasticbeanstalk:cloudwatch:logs"
        name = "RetentionInDays"
        value = var.logs_retention
      },
      {
        namespace = "aws:elasticbeanstalk:application:environment",
        name = "SPRING_PROFILES_ACTIVE",
        value = "aws"
      }
  ]
}

variable "enable_logging" {
  type    = bool
  default = true
}

variable "delete_logs_on_terminate" {
  type    = bool
  default = true
}

variable "logs_retention" {
  type    = number
  default = 30
}

variable "newapp_db_name" {
  type    = string
  default = "newapp"
}

variable "newapp_db_engine" {
  type    = string
  default = "postgres"
}

variable "newapp_db_engine_version" {
  type    = string
  default = "15.3"
}

variable "newapp_db_instance_class" {
  type    = string
  default = "db.t3.small"
}

variable "newapp_db_storage" {
  type    = number
  default = 10
}

variable "newapp_db_user" {
  type    = string
  default = "root"
}

variable "newapp_db_password" {
  type  = string
default = null

}

variable "newapp_db_random_password" {
  type    = bool
  default = true
}

variable "newapp_db_multi_az" {
  type    = bool
  default = false
}

